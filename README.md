# Codebase release r1.0 for amcheck

by Andriy Smolyanyuk, Libor Šmejkal, Igor I. Mazin

SciPost Phys. Codebases 30-r1.0 (2024) - published 2024-06-12

[DOI:10.21468/SciPostPhysCodeb.30-r1.0](https://doi.org/10.21468/SciPostPhysCodeb.30-r1.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.30-r1.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Andriy Smolyanyuk, Libor Šmejkal, Igor I. Mazin.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.30-r1.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.30-r1.0)
* Live (external) repository at [https://github.com/amchecker/amcheck/tree/v1.0.0](https://github.com/amchecker/amcheck/tree/v1.0.0)
